import React from 'react';

const Card = ( { name, email, username, id } ) => {
    //const { name, email, id } = props;
    return(
        <div className='bg-light-green dib pa3 br3 grow ma2 bw2 shadow-5'>
            <img src={`https://robohash.org/${id}?200x200 `} alt='robot' />
            <div>
                <h2>{name}</h2>
                <h4>{username}</h4>
                <p>{email}</p>
            </div>
        </div>
    );
}
export default Card;